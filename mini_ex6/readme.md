[run.me](https://gl.githack.com/mettessancen/ap2019-mette-buch/raw/master/mini_ex6/empty-example/index.html)

This week i had a really hard time understanding the new syntaxes - therefore i have concentrated on using the techniques: arrays, classes and objects, instead of making a meaningful game.
My program is a generator that can make a sentence out of words from three **cat**egories. The random sentence is being generated when clicking the button - you can click and generate new sentences infinite times. Furthermore you can decorate the screen with small cat symbols in different colours. 
I have used the Shiffman videoes to understand the new way to program - creating a class and from that creating objects.