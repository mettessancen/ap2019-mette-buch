var cats = ["brown cat", "black cat", "big cat", "small cat", "white cat", "asian cat", "cowboy cat", "crazy cat", "nyan cat", "jazz cat"];
var does = ["smells like", "likes to eat", "looks like", "dreams about", "works with", "likes to sit on", "wants to look like", "licks", "likes to push", "stares at"];
var things = ["an iPhone", "poop", "strawberry jam", "vanilla pudding", "a blue dragon", "Kanye West", "a private jet", "an easter bunny", "a ketchup buttle", "a french bulldog"];

var miavs = [];



function setup() {
  createCanvas(700, 700);
  background(245, 241, 220);
}


function mousePressed() {
  var m = new miav(mouseX, mouseY, random(0, 255), random(0, 255), random(0, 255));
  miavs.push(m);
}

function mouseClicked() {
  if (mouseX > 200 && mouseX < 500 && mouseY > 130 && mouseY < 170 ){

    clear();

    background(245, 241, 220);

  push();
 		textAlign(CENTER);
    textSize(23);
    fill(0);
    text(random(cats), 120, 348);
    text(random(does), 350, 348);
    text(random(things), 575, 348);
	pop();

  }
}

function draw() {


  for (var miav of miavs) {
    miav.appear();
  }

  for (var i = 0; i < miavs.length; i++) {
    miavs[i].appear();
  }

push();
 	stroke(0);
  strokeWeight(2.1);
  rectMode(CENTER);
  fill(381, 123, 150);
  rect(350, 150, 300, 40, 3);

  textAlign(CENTER);
  textSize(16)
  text("C A T   G E N E R A T O R", 350, 155);
pop();

  line(25, 350, 225, 350);
  line(250, 350, 450, 350);
  line(475, 350, 675, 350);

push();
  stroke(255);
  textSize(16)
	fill(0);
  textAlign(CENTER);
  text("*decorate the canvas with beautiful cats", 350, 675);
pop();



}

class miav {
  constructor(x, y, c1, c2, c3) {
    this.x = x;
    this.y = y;
    this.c1 = c1;
 		this.c2 = c2;
     this.c3 = c3;
  }
  appear() {
    fill(this.c1, this.c2, this.c3);
    text("ᄼーᄾ\n  ° ѫ ° ", this.x, this.y);
}
}
