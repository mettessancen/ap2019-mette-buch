[runme](https://gl.githack.com/mettessancen/ap2019-mette-buch/raw/master/mini_ex4/empty-example/index.html)

With this program i have tried to make a simple statement from what i have learned from this weeks litterature: ''The like economy: Social buttons and the data-intensive web" by Caroline Gerlitz and Anne Helmond.
The program shows the front page on facebook. When you click the 'opret profil'-button the webcamera turns on.
I was inspired by the articles take on facebooks power and how  users have no choice on what info to give to facebook. I think the article was interesting because it was not about surveillance - a really current topic and my least favorite topic for that matter. Rather it was more about how much power facebook has because it has so much data about everybody, both users, companies and webmasters. The reason for this is that facebook has strict conditions if webmasters wants to use their services - like the share button - and with facebooks spread in the world, companies have no choice but to implement facebooks buttons.
I rarely read conditions when i sign up for new webpages. I am aware that i am giving out my data - but not how much data and more importantly what my apathetic attitude means to the like economy.
In other words: when you press the 'sign me up'-button you are letting facebook know your every move. In my program 'every move' is symbolized through the concrete moves of your body - what is in front of the web camera. 
The web camera-input is not a comment on whether facebooks moniters its users over web cameras or not * putting on my tin foil hat *.

This week i had a lot of issues with implementing the webcamera input the right way. I had higher ambitions with this weeks mini_ex - like implementing buttons over the blue words.