[runme](https://gl.githack.com/mettessancen/ap2019-mette-buch/raw/master/mini_ex8/empty-example/index.html)

“We; Mette, Stine and Sophie, have chosen to work on a Haiku generator, creating longing- and heartfelt, nature poems, through pseudo-random generator on computational devices. 

A haiku, which is a Japanese poem, is usually the intermediary of a theme and a setting. These themes and settings must be described so well that they stand completely sharp for the reader. Often the settings, themes and the nature presented are not obviously related, this can help the reader create their own meaning of the Haiku and interpret the different components of it - eg. forms contrast. 

This in itself is a paradox, and we’ve spent quite some time discussing what the generation progress does to the art-form of creating these Haiku poems. Using a machine as a medium to convey feelings, or to put various lines and rhythms together will possibly create some absurd poems, or lines that we wouldn’t necessarily put together – which we feel conveys the writings and poetry of modern society. It’s not enough to convey your feelings, or pour your heart into a book, the pseudorandom aspect of our code/ poem generator is taking a spin on Haiku, making it more humorous and “random”.

We are using arrays to group lines of the poem, that we’ve written, or taken from various other Haiku books, and generators online.
One group of arrays with 5 syllables, with nature statements
The second group of arrays is with 7 syllables, focused on describing actions and movements. 
The third group of arrays has 5 syllables, as the first one, and the main focus in this group is to have an open ending and be intriguing.

With the background and colour spectrum of the generator, we’ve tried to create an ephemeral look, fragile but coherent – trying to make the poems the centre of attention. Furthermore, the backgrounds’ simplicity serves as a contrast to the very descriptive poems. 
With these choices, we are trying to pay homage to the roots of Haiku, being something beautiful to frame and share with people.”

My thoughts when making our program was mainly on the randomness-factor. Our group recently did presentation about randomness, where we discussed pseudo randomness. I think it was interesting to mix this with poetry because art is thought trough and have many reflections behind it. Our haiku poem is generated on basis of our data which we randomly came up with – only following the haiku rules of 5-7-5 syllables. Therefore, a haiku poem with no artistic thoughts behind are being generated. I think this contrast is interesting – especially when it comes to aesthetic programming because in this class, we are making programs with deeper meanings, and the artistic thoughts often clashes with the hardcore and strict coding rules. 
