var hudfarveR;
var hudfarveB;
var hudfarveG;


function setup() {
  createCanvas(600, 600);
}

function draw() {
  background(255);

  hudfarveR = random(255);
  hudfarveB = random(255);
  hudfarveG = random(255);

  ellipseMode(CENTER);
	fill(hudfarveR, hudfarveB, hudfarveG);
	ellipse(300, 300, 330, 330);


	fill(0);
 textSize(250);
 textAlign(CENTER);
 text('⏝', 300, 280);

  	fill(0);
 textSize(250);
 textAlign(CENTER);
 text('. .', 300, 270);


  }

function mousePressed() {
  noLoop();
  }

function mouseClicked() {
  loop();
  }
