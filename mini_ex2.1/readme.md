[run me](https://gl.githack.com/mettessancen/ap2019-mette-buch/raw/master/mini_ex2.1/empty-example/index.html)

OBS: the screenshot(emoji#1.png) is in the folder 


**Mini_ex2.1 reflection**

The program consists of a traditional smiley. The skincolor of the smiley changes quickly. When the mouse is being pressed and hold down the skincolor stops changing. 
I have used the following syntax:
-	background
-	var
-	creteCanvas
-	ellipsemode
-	ellipse
-	fill
-	textsize
-	textAlign
-	text
-	noLoop
-	loop

This program is my comment to the debate on how the skin color options with emojis create problems in society. When using randomness as the only factor to choose skin color, I think it would be hard for anybody to feel discriminated.
