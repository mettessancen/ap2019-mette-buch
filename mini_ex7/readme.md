[runme](https://gl.githack.com/mettessancen/ap2019-mette-buch/raw/master/mini_ex7/empty-example/index.html)

	
For this program i was inspired by something i do a lot: doodling on paper - especially when i am bored. I often state rules for my sketching like: these colours cannot touch eachother or draw a ring for each centimeter. For this program i was inspired by one of my "fantasy rules": fill out the paper with even continued lines.
The colour is chosen to simulate the ink-colour in a pen. 

The rule in my program is that a line is being drawed from one random point to another random point - where the line ended (point nr. 2) a new line starts and ends in another random point - an so on. This becomes an eternal writing of coherent lines.
The lines keep being drawed, filling out the canvas more and more. The stroke is as thin as possible making the process of filling the canvas with lines protracted. 
Even though the program runs for a long time, it is still possile to see small areas of the canvas - there occurs a "need" with the user for the canvas to be filled out - but it seems to be impossible because of the thin line. 
In this program there is no end product - it can go on forever - but will eventually fill out the canvas. 

The rule is very simple and is alone in this program. Therefore the visual output is as well. 

Generativity and automatism is about giving the machine power - or at least a role in a process. From giving simple instructions that cannot generate something unexpected (like my program) to generative art where the machine is in control generating for example sounds/music. 
