//billeder:
let blad;
let behive;
let bi;

//lyde:
var buzz;

function preload() {
  bi = loadImage('assets/bi.png');
  blad = loadImage('assets/blad.png');
  behive = loadImage('assets/behive.png');
}

function setup() {
createCanvas(1000, 800);
background(100,185,234);
buzz = createAudio('assets/buzz.mp3');
}


function draw() {
	fill(85,52,0);
	stroke(85,52,0);
  triangle(0, 85, 0, 120, 400, 90);
	quad(300, 95, 340, 140, 440, 180, 330, 150);
	quad(150, 30, 70, 30, 35, 100, 70, 40);

  image(blad, 30, 107, 65, 65);
  image(behive, 290, 160, 300, 300);

  fill(0);
  ellipse(440, 376, 50, 50);
}

function mousePressed() {
  let d = dist(mouseX, mouseY, 440, 376);
  if (d < 25) {
  image(bi, random(50, 750), random(50, 950), random(25, 35), random(22, 38));
}
}

function mouseClicked() {
  let d = dist(mouseX, mouseY, 440, 376);
  if (d < 25) {
  buzz.play();
}
}
