OBS: kan kun åbnes i Firefox!

MINI_EX1 REFLECTION:

When I tried to figure out what my first code should be, I focussed on the references I thought were interesting in the P5.js reference list. I decided to include shapes, colors, images, interaction via mouseclicking and sound. 
The process has been frustrating which shows in the aesthetic of the code. My focus was to make the code work - especially the interaction, which was really hard to incorporate – not to make it aesthetic. I really regret this in my work process. On the other hand – I learned a lot more than I thought I would, because my ambitions grew while I was in the process.
The hardest part in my process was to incorporate the interactive part. The way I got the right result was though trial and error. I thought that the P5 website wasn’t really helpful with my issues in this area.
The only thing I could manage to figure out was how to decide which elements would be in the front and which to be in the back. This made me reflect upon how the software actually reads and execute the code. This software issue was especially hard for me, because I am used to work in CS-programs like Photoshop and InDesign, which uses ‘Layers’ as indicators. 
It was interesting for me to work in a context where lines of text create visual results – and where I could not ‘drag’ and ‘draw’, but actually had to write my way to the result within a certain syntax. 
My conclusion after this work proces is that as soon I really understand the rules I will begin to feel safe – and that is from where my ambitions and creativity grow. 

