//declaring variables for images used and keys pressed by the user
var img;
var typed_keys;

function preload () {
  //preloading pictures so that the user of the program shouldn't wait for every single picture to load when inserted into canvas

  //angry
img1 = loadImage('angry.png')

  //happy
img2 = loadImage("happy.png")

  //surprised
img3 = loadImage("surprised.png")

  //sad
img4 = loadImage("sad.png")

  //laughing
img5 = loadImage("laughing.png")

  //mindblown
img6 = loadImage("mindblown.png")

  //sick
img7 = loadImage("sick.png")

  //rolling eyes
img8 = loadImage("Frustrated.png")

  //sleepy
img9 = loadImage("sleepy.png")

//keyboard shortcuts instruction sheet
img10 = loadImage("shortcuts.png")

//message
img11 = loadImage("message.jpg")

//keyboard
img12 = loadImage("keyboard.png")

}

function setup() {
typed_keys = "";

createCanvas(windowWidth, windowHeight);
background(255);

//building the actual phone with rectangles, ellipses and text
  fill(0,0,0);
  rectMode(CENTER);
  rect(350, 350, 330, 600, 20);


  fill(255,255,255);
  rect(350, 355, 270, 480);


  strokeWeight(4);
  stroke(51);
  fill(0,0,0);
  ellipse(350, 622, 45, 45);

  strokeWeight(1);
  line(485, 170, 215, 170);

  textSize(20);
  text('Winnie', 323, 160);

//message
  image(img11,224,338, img11.width/4, img11.height/4);

  textSize(13);
  text('When is the mini_ex due?', 243, 370);

//instruction sheet
  image(img10,585,168, img10.width/2, img10.height/2);

//keyboard
  image(img12,215,437, img12.width-35, img12.height-10);


}

function keyTyped() { //checking which keys have been pressed by the user
  typed_keys+=key;
  var last_two_keys = typed_keys.substring(typed_keys.length - 2); //returns a new string

  //the logic of the program. When two particular keys are pressed, an image will be shown

  //angry
  if (last_two_keys == ":@") {
    image(img1,400,342, img1.width/17, img1.height/17);

  }

  //happy
  else if (last_two_keys == ":D") {
    image(img2,396,345, img2.width/18, img2.height/18);

  }

  //surprised:
  else if (last_two_keys == ";o") {
    image(img3,400,345, img3.width/18, img3.height/18);

  }

  //sad
  else if (last_two_keys == ":(") {
    image(img4,400,347, img4.width/14, img4.height/14);

  }

  //laughing
  else if (last_two_keys == ";D") {
    image(img5,400,345, img5.width/14, img5.height/14);

  }

  //mindblown
  else if (last_two_keys == "mb") {
    image(img6,400,345, img6.width/17, img6.height/17);

  }

  //sick
  else if (last_two_keys == ":c") {
    image(img7,400,345, img7.width/19, img7.height/19);

  }

  //rolling eyes
  else if (last_two_keys == "z;") {
    image(img8,400,348, img8.width/18, img8.height/18);

  }

  //sleepy
  else if (last_two_keys == "zz") {
    image(img9,390,342, img9.width/15, img9.height/15);

  }

}
