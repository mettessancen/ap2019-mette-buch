# **MINI_EX10**



### **Individual work:**

![Untitled_Diagram__2_](/uploads/aa78af83557067e39f077266ef1abbce/Untitled_Diagram__2_.jpg)

[Link to project folder](https://gitlab.com/mettessancen/ap2019-mette-buch/tree/master/mini_ex6)


I had many considerations when doing the flowchart. I do not think that this kind of code works well for doing a flowchart because there is no end goal. I was not sure on how I should incorporate the arrays of data for the sentence without making it look too complicated and not take up too much space in the flowchart 

__________________________________________________________________________________________________________________________________________________________________

### **Group work:**


**IDEA 1**

      Truemoji – what do you mean?
            -	The signal value of emojis

In this program the user is presented with a neutral text massage. For each sentence the user can insert an emoji from a dropdown menu and thereby personalize the message with the emojis the user think is fitted for the text massage. When emojis from all the dropdown-menues has been chosen the user can click the send-button.

The emojis to choose from in the program is from IOS text message app. When you write a feeling in the text box and switch to the emoji keyboard, the IOS will propose the emoji it think is fitted best for the feeling. 5 of the feelings had many options to choose from – those are the feelings/categories the user can choose from in the program. 
*The 5 feelings/categories are placed with each sentence randomly. 

One of the two pages is now shown (we in group have not yet decided which output we want in the program):

Option 1:
The user gets a reply: a message back from a clearly hurt recipient

Option 2: 
A collage of other users personalized text-messages. 
   -How did other people perceive the written text?

![flowchart_idea_1](/uploads/12bd96898fd58f7928c33826f951c1ee/flowchart_idea_1.jpg)


To make this program we will be doing a small field study consisting of simple interviews. All of the 30 emojis we use in the program only represent 5 different feelings according to IOS. The 30 emojis will be represented to the participants in the study and the participants are asked to tell their first reaction/impression. 
       - Given the different impressions internally in our group, we expect that our studies will show that people use the emojis in very different ways/contexts. 

Prototype in Axure:  [link to prototype](https://cz17yw.axshare.com)








**IDEA 2**

           Capmojis
               - The most including cap app for making emojis of yourself. Express your true feelings

In this program the user is presented with a textbox where the user is able to write an optional text. When the user is finished, he/she will click the emoji-button and a picture of the user is being taken. The user is now free to send the text. 
When the send button is pressed, the message written by the user will appear on the replicants side with an emoji of your face. This way the emoji will reflect your true feeling/look while communicating via text. 

![flowchart_idea_2](/uploads/a1d9d6809307118335530ff5554a8ebb/flowchart_idea_2.jpg)







**What might be the possible technical challenges for the two ideas and how are you going to solve them?**


**Technical challenges for the first idea:**
* To avoid aesthetic disagreements during the coding process, we will make an aesthetical prototype of our program in Axure (link under the flowchart)
* Another predicted technical challenge in this design process will occur if we choose option number 2. If we choose option number 2 the output of the program, will show a collage filled with other peoples messages including your own, to make it possible for the user to compare his/hers message and signal value of the message with others. We would like to be able to capture different users messages, and we actually don’t know how to solve this challenge. 
* The last technical challenge of the program, will probably be the interactive aspect. For example to connect a button with the different emojis, and let them disappear after the user have chosen a specific emoji. 




**Technical challenges for the second idea:**

* To shape and make the “emoji” in which your face will show  
* How to go about the aesthetic limitations of trying to fit a whole face inside the original emoji format. 
* How to keep the data capture safe, and prevent other users of the app to get hold of your personal smileys. It’s also a matter of copyright? Who has the ownership of the personal emojis? (is it even possible to create this app?) 
* The last technicality would be how to implement the capmojis in the texts. Our first thougts on how to go about it, is to simply have an emoji button; when pressed, it takes a picture of one’s expression and “emotions” and fits it into the original emoji format. Then we have to get the camera and the message app to work together, and figure out wether to store some of the capmojis as the standards, or wether to always create new ones, deleting the old. 

*We are going to solve the technical challenges by watching the relevant Shifman-videoes. Furthermore we will be getting help and verbal sparring from our mentors, instructors and classmates.*



____________________________________________________________________________________________________________________







**Individual: How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?**
My individual flowchart is different from our group-flowchart in the way that there is no end-goal in my individual code. In the group program the user has to make decisions and think about their choices. When all the choices are filled out the user can press “send” and get the feedback image, which is also the end goal = the place where to designer wants the user to go. The flowcharts for the group programs are strict, even though there are options to make. The options are being put into a broader perspective where the user is being out in a position where he/she has to think about the choices they just made.
In my individual program there is no determined end-goal. The output should just be visually fun. The user’s decisions to make are extremely simple in form of the cat-symbols the user can place. The user cannot make specific sentences because the generator chooses the words randomly from the code. Therefore the flowcharts role is rather to give an overview of the possibilities, more than a guideline to use the program. 









**Individual: If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms?**
In the class about algorithms we discussed the flowchart that showed the process of baking a cake. There was few and simple process-steps and only one time the user should make a decision. It is quite clear to me that there aren’t many decisions to make when baking a cake – is it done or not? The banality of the flowchart made me think about where else we could make flowcharts/algorithms in our daily life – and what could we learn from it? If I had to make an algorithm over my whole day, I think I would be surprised over my habits and the way I do things and solve tasks in my everyday life. Could I improve my way of life my getting a flowchart-like overview?
Even though algorithms are great for getting overview and visually simplifying processes, there would be to many factors in everyday life for a person to make one over their day. What if the algorithm said that you had to bicycle to school, but is was raining so you took the bus? – the consequences of unpredicted factors is way too overwhelming for this to ever work, I think. 





