[run me](https://gl.githack.com/mettessancen/ap2019-mette-buch/raw/master/mini_ex2.2/empty-example/index.html)

OBS: the screenshot (emoji#2.png) is in the folder

**Mini_ex2.2 reflection**

*I had issues uploading images to Atom, therefore the background image is missing. The code works as intended. The background image is uploaded in the folder named emojiCollage.jpg.*

This program consists of a traditional smiley made of simpel symbols. This is my take on ‘Multi’ by David Reinfurt. This classic emoji is surrounded by all the new emojis from IOS. The emoji cries when the mouse is clicked.

I have used the following syntax:

-
-
-
-
-

When reading and discussing all the issues the emoji possibilities has made, what made me think the most was if anybody thought of these consequences when developing new emojis. What would the old emojis (and the creators of these) think of today’s debate about discrimination through emojis? Maybe it would cry because of all the problems it has made?



